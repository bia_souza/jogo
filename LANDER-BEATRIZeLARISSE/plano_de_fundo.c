#include <SOIL/SOIL.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

GLuint texturaFundo;

void inicializa_plano_de_fundo(void){
	texturaFundo = SOIL_load_OGL_texture("fundo.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
	if (texturaFundo == 0) {
		printf("Erro do SOIL: '%s'\n", SOIL_last_result());
	}
}

void desenhaFundo(void) {
	glColor3f (1.0, 1.0, 1.0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texturaFundo);
	glBegin(GL_TRIANGLE_FAN);
		glTexCoord2f(0, 0); glVertex2f(0, 0);
		glTexCoord2f(1, 0); glVertex2f( 500, 0);
		glTexCoord2f(1, 1); glVertex2f( 500,  500);
		glTexCoord2f(0, 1); glVertex2f(0,  500);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}

const float GRAVITY = -0.8;
const float LOST_FUEL = 0.2;

void Teclado (unsigned char key, int x, int y) {
	switch (key) {
		case 27: 
			exit(0); break;
		case 'P':
		case 'p':
		break;
		case 'r':
		case 'R':
			break;
	}
} 

void tecladoEspecial(int key, int x, int y) {
	switch (key) {
		case  GLUT_KEY_LEFT: 	
			Apolo.angle += 15;
			Apolo.x +=(sin(360+Apolo.angle*(2*M_PI)*GRAVITY))+Apolo.velocidade_nave.x;
		break;
		case GLUT_KEY_RIGHT:
			Apolo.angle += -15;
			Apolo.x -=(sin(360+Apolo.angle*(2*M_PI))*(GRAVITY))+Apolo.velocidade_nave.x;	
		break;
		case GLUT_KEY_UP:
			ligado = 1;
			if (forca_motor < 8.0)
				forca_motor += 1.5;
			
			Apolo.fuel -=LOST_FUEL*0.2;
	
			Apolo.velocidade_nave.x = sin(360+Apolo.angle*(2*M_PI)*forca_motor);
			Apolo.velocidade_nave.y = cos(360+Apolo.angle*(2*M_PI)*forca_motor);
			
			if (Apolo.angle == 0.0) Apolo.y += (cos(360+Apolo.angle*(2*M_PI))*GRAVITY)-Apolo.velocidade_nave.y;
			else if(Apolo.angle > 0.0) {
				Apolo.x -=(sin(360+Apolo.angle*(2*M_PI)*GRAVITY))+Apolo.velocidade_nave.x;	
				Apolo.y += (cos(360+Apolo.angle*(2*M_PI))*GRAVITY)-Apolo.velocidade_nave.y;	
			}
			else {
				Apolo.x +=(sin(360+Apolo.angle*(2*M_PI)*GRAVITY))+Apolo.velocidade_nave.x;	
				Apolo.y += (cos(360+Apolo.angle*(2*M_PI))*GRAVITY)-Apolo.velocidade_nave.y;	
			}		
		break;
		default:
			ligado = 0;
			forca_motor = 0.0;
		break;
	}
	glutPostRedisplay(); 
}

#include<stdio.h>
#include<stdlib.h>
#include<GL/glew.h>
#include<GL/freeglut.h>
#include<GL/glut.h>

/*Se a nave colidiu com o plataforma, retorna 1,
*retorna zero se nao tiver colidido.
*/
int verificaColisaoPlataforma(void) {
	const float aproximacaoX = 30.0;
	const	float aproximacaoY = 30.0;
	if (abs(Apolo.y-pouso.y)<aproximacaoY && abs(Apolo.x-pouso.x)<=aproximacaoX) return 1;
	else return 0;	
}
/*Essa função verifica se a nave colidiu com um dos lados ou com o chão.
* Antes, verifica se ela colidiu com a plataforma.
* Caso não tenha colidido, retorna 0 se não houve colisão com o chão ou com as paredes
* e 1 se tiver tido algum tipo de colisão com o chão ou as paredes.
*/
int verificaColisaoChaoParede(void) {
	float aproximacaoY = 20.0;
	if (!verificaColisaoPlataforma() && Apolo.y <=aproximacaoY) return 1;
	else if (!verificaColisaoPlataforma() && Apolo.x >= aproximacaoX2) return 1;
	else if (!verificaColisaoPlataforma() && Apolo.x <= aproximacaoX1) return 1;
	else return 0;
}

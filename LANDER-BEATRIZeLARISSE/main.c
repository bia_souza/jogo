#include<stdio.h>
#include<time.h>
#include<math.h>
#include<stdlib.h>
#include<GL/glew.h>
#include<GL/freeglut.h>
#include<GL/glut.h>
#include<string.h>
#include<SOIL/SOIL.h>

#include"inicializa_nave.h"
#include"inicializa_plataforma.h"
#include"teclado.h"
#include"colisoes.h"
#include"plano_de_fundo.h"
#include"fim_de_jogo.h"
#include"display.h"

const float GRAVIDADE = -1.0;
const float PERDA_FUEL = 0.02;
const float GANHO_FUEL = 3.0;
int colisaoParede = 0, colisaoPlataforma = 0;
void desenhaCena() {
   	 glClear(GL_COLOR_BUFFER_BIT);
		colisaoParede = verificaColisaoChaoParede();
		colisaoPlataforma = verificaColisaoPlataforma();
		if (colisaoParede == 1 || colisaoPlataforma == 1) desenhaFimDeJogo();
		else desenhaFundo();
		desenhaPlataforma();
		desenhaNave();
		displayMensagem();
	glFlush();
}
void inicializa() {
	Inicializa_plataforma();
	Inicializa_nave(GRAVIDADE);
	inicializa_fim_de_jogo();
	inicializa_plano_de_fundo();
	glClearColor (0.0, 0.0, 0.0, 0.0);	
}
void atualiza_jogo() {
	//Apolo.fuel -= PERDA_FUEL;
	if (ligado == 0)  {
		Apolo.y -= (cos(360+Apolo.angle*(2*M_PI))*GRAVIDADE);	
		if (Apolo.angle > 0.0 ) Apolo.x +=(sin(360+Apolo.angle*(2*M_PI)*GRAVITY))+Apolo.velocidade_nave.x;
		else if (Apolo.angle < 0.0) Apolo.x +=(sin(360+Apolo.angle*(2*M_PI))*(GRAVITY))+Apolo.velocidade_nave.x;	
	}

	mudaTexturaNave(ligado);	
	if (Apolo.fuel == 0 ) {
		exit(0);
	}
	
	while (ligado == 0 && forca_motor>0.0) {
		forca_motor -= 0.2;
	}

	if (ligado == 0) {
		Apolo.velocidade_nave.x = 0.0;
		Apolo.velocidade_nave.y = 0.0;
	}
	ligado = 0;
	
	glutTimerFunc(25, atualiza_jogo, 0);	
	glutPostRedisplay();
}

void redimensiona() {
	glViewport(0, 0, 500, 500);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho (0,  500, 0, 500, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

int main(int argc, char **argv) {
	glutInit(&argc, argv);

	glutInitContextVersion (1, 1);
    	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);	

	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize (500, 500);
	glutInitWindowPosition (500, 100);
		
	glutCreateWindow("Meu primeiro joguinho");
		glutDisplayFunc(desenhaCena);	
		glutTimerFunc (0, atualiza_jogo, 30);	
		glutReshapeFunc(redimensiona);
		glutSpecialFunc(tecladoEspecial);	
		glutKeyboardFunc(Teclado);	
		inicializa();
	glutMainLoop();

	return 0;
}

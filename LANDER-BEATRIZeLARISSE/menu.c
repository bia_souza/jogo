#include<stdio.h>
#include<time.h>
#include<math.h>
#include<stdlib.h>
#include<GL/glew.h>
#include<GL/freeglut.h>
#include<GL/glut.h>
#include<string.h>
#include<SOIL/SOIL.h>

void Menu(int value);

GLuint texturaMenu;


void inicializa_plano_de_fundo(void){
	texturaMenu = SOIL_load_OGL_texture("star.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
	if (texturaMenu == 0) {
		printf("Erro do SOIL: '%s'\n", SOIL_last_result());
	}
}

void desenhaFundo(void) {
	glColor3f (1.0, 1.0, 1.0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texturaMenu);
	glBegin(GL_TRIANGLE_FAN);
		glTexCoord2f(0, 0); glVertex2f(0, 0);
		glTexCoord2f(1, 0); glVertex2f( 500, 0);
		glTexCoord2f(1, 1); glVertex2f( 500,  500);
		glTexCoord2f(0, 1); glVertex2f(0,  500);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}

void display() {
   	 glClear(GL_COLOR_BUFFER_BIT);
	desenhaFundo();
	glFlush();
}

int main(int argc, char *argv[])
{
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowPosition(200,250);
	glutInitWindowSize(500,400);
	glutCreateWindow("Menu");
	
	inicializa_plano_de_fundo();
	glutDisplayFunc(display);
	
	int menu = glutCreateMenu(Menu);

	glutAddMenuEntry("Jogar",1);
	glutAddMenuEntry("Instrucoes",2);
	glutAddMenuEntry("Sobre",3);
	glutCreateMenu(Menu);
	glutAddSubMenu("Menu",menu);

	glutAttachMenu(GLUT_LEFT_BUTTON);

	glutMainLoop();

	return 0;
}

void Menu(int value){
	switch(value){
		case 1:
			break;

		case 2:
			break;

		case 3:
			break;

	}

	glutPostRedisplay();
}

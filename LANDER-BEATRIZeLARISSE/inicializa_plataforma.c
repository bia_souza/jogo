#include<stdio.h>
#include<time.h>
#include<math.h>
#include<stdlib.h>
#include<GL/glew.h>
#include<GL/freeglut.h>
#include<GL/glut.h>
#include<SOIL/SOIL.h>

int height_plat;
int width_plat;
GLuint texturaPlataforma;

struct plataforma {
	float x;
	float y;
};

struct plataforma pouso;

void Inicializa_plataforma(void) {
	int width_screen = glutGet(GLUT_WINDOW_WIDTH);
	height_plat = 0;

	srand(time(NULL));	
	width_plat = rand()%width_screen;

	while (width_plat > width_screen)
		width_plat = rand()%width_screen;
	
	if (Apolo.y < width_plat) pouso.x = width_plat-Apolo.y; 
	else pouso.x = Apolo.y-width_plat;

	pouso.y = height_plat;
	texturaPlataforma = SOIL_load_OGL_texture("pouso.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}
void desenhaPlataforma (void) {	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	if (!texturaPlataforma) printf("Erro do SOIL: '%s'\n", SOIL_last_result());

	glColor3f(1, 1, 1);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texturaPlataforma);

	glPushMatrix();
		glLoadIdentity();
		glTranslatef(pouso.x, pouso.y, 0);
		glBegin(GL_TRIANGLE_FAN);
			glTexCoord2f(0, 0); glVertex2f (-30, -30);
			glTexCoord2f(1, 0); glVertex2f (30, -30);
			glTexCoord2f(1, 1); glVertex2f (30, 30);
			glTexCoord2f(0, 1); glVertex2f (-30, 30);
		glDisable(GL_TEXTURE_2D);
	    	glEnd();
	glPopMatrix();
}

#include<stdio.h>
#include<time.h>
#include<math.h>
#include<stdlib.h>
#include<GL/glew.h>
#include<GL/freeglut.h>
#include<GL/glut.h>
#include<SOIL/SOIL.h>


GLuint* texturaNave;
GLuint texturaNaveOFF;
GLuint texturaNaveON;

float aproximacaoX1= 20.0; 
float aproximacaoX2= 480.0;
float forca_motor = 0.0;
int ligado = 0;

struct velocidade {
	double x;
	double y;
};
struct nave {
	float fuel; //combustivel
	float y; //altura
	float x; //largura
	struct velocidade velocidade_nave;
	GLfloat angle;
};

struct nave Apolo;

void mudaTexturaNave (int x) {
	if (x == 0) {
		texturaNave = &texturaNaveOFF;
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else {
		texturaNave = &texturaNaveON;
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
}
void Inicializa_nave(float GRAVIDADE) {
	mudaTexturaNave(ligado);	
	int width_screen = glutGet(GLUT_WINDOW_WIDTH);
	int height_screen = glutGet(GLUT_WINDOW_HEIGHT);

	srand(time(NULL));
	
	Apolo.x = rand()%width_screen;
	Apolo.y = rand()%height_screen;

	while (Apolo.y < height_screen/2)
		Apolo.y = rand()%height_screen;
	while (Apolo.x <aproximacaoX1 || Apolo.x >aproximacaoX2)
		Apolo.x = rand()%width_screen;

	Apolo.fuel = 500.0;
	Apolo.angle = 0.0;
	
	Apolo.velocidade_nave.x = 0.0;
	Apolo.velocidade_nave.y = 0.0;
	forca_motor = 0.0;

	texturaNaveOFF = SOIL_load_OGL_texture("nave1.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	texturaNaveON = SOIL_load_OGL_texture("nave2.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	if (!texturaNaveON || !texturaNaveOFF) printf("Erro do SOIL: '%s'\n", SOIL_last_result());
}

void desenhaNave (void) {
	glColor3f (1.0, 1.0, 1.0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, *texturaNave);

	glPushMatrix();
	glLoadIdentity();
	
	glTranslatef(Apolo.x, Apolo.y, 0);
          glRotatef(Apolo.angle, 0, 0, 1);
	
	glBegin (GL_POLYGON);
		glTexCoord2f(0, 0); glVertex2f (-20, -20);
		glTexCoord2f(1, 0); glVertex2f (20, -20);
		glTexCoord2f(1, 1); glVertex2f (20, 20);
		glTexCoord2f(0, 1); glVertex2f (-20, 20);
	glDisable(GL_TEXTURE_2D);
	glEnd();
	glPopMatrix();
}

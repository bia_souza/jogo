#include<stdio.h>
#include<time.h>
#include<math.h>
#include<stdlib.h>
#include<GL/glew.h>
#include<GL/freeglut.h>
#include<GL/glut.h>
#include<string.h>
#include<SOIL/SOIL.h>

void mensagem (void* fonte, char* palavra, double valor, double x, float y) {
	glRasterPos2f(x, y);
	char c_valor[10];
	sprintf(c_valor, "%6.2f", valor);

	for (int i=0; i < strlen(palavra); i++) 
		glutBitmapCharacter(fonte, palavra[i]);
	if (valor != 1000) {
		for (int i=0; i < strlen(c_valor); i++) 
			glutBitmapCharacter(fonte, c_valor[i]);
	}
}
void displayMensagem(void) {
	glColor3f(1.0,1.0,1.0);
	mensagem(GLUT_BITMAP_TIMES_ROMAN_10, "VelocidadeX: ", Apolo.velocidade_nave.x, 10, 480);
	mensagem(GLUT_BITMAP_TIMES_ROMAN_10, "VelocidadeY: ", Apolo.velocidade_nave.y, 10, 460);
	mensagem(GLUT_BITMAP_TIMES_ROMAN_10, "Combustivel: ", Apolo.fuel, 10, 440);
	mensagem(GLUT_BITMAP_TIMES_ROMAN_10, "Horizontal: ", Apolo.x, 400, 480);
	mensagem(GLUT_BITMAP_TIMES_ROMAN_10, "Vertical: ", 	Apolo.y, 400, 460);
	mensagem(GLUT_BITMAP_TIMES_ROMAN_10, "Horizontal: ", pouso.x, 400, 440);
	mensagem(GLUT_BITMAP_TIMES_ROMAN_10, "Vertical: ", pouso.y, 400, 420);
	glFlush();
}
